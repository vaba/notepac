% NOTEPAC(1) notepac 1.0
% vaba
% June 2021

# NAME
notepac - make notes about package

# SYNOPSIS
**notepac** \[*OPTIONS*\]

# DESCRIPTION
notepac allows users to make notes about packages on their system, especially useful for those rarely used, or obscurely named packages

# OPTIONS
**-i** *COMMAND*
: install the PACKAGE with COMMAND, and make a NOTE about the PACKAGE

**-p** *PACKAGE* 
: PACKAGE to interact with 

**-n** *NOTE*
: NOTE to write about the given PACKAGE

**-v**
: show the note made about the PACKAGE

**-r** *COMMAND*
: remove the PACKAGE with COMMAND, and delete the note made about the PACKAGE

**-x**
: delete the note made about the PACKAGE

# EXAMPLES

**notepac -i "sudo pacman -S" -p "firefox" -n "web browser"**
: install the package "firefox" using the command "sudo pacman -S", and assign the note "web browser" to the package "firefox"

**notepac -p "firefox" -n "web browser"**
: assign the note "web browser" to the package "firefox"

**notepac -p "firefox" -v**
: display the note associated with the package "firefox" (if it exists)

**notepac -r "sudo pacman -R" -p "firefox"**
: uninstall the package "firefox" using the command "sudo pacman -R", and delete the note associated with the package "firefox" (if it exists)

**notepac -p "firefox" -x**
: delete the note associated with the package "firefox" (if it exists)

# BUGS
No known bugs

# AUTHOR
aur.demifusion@8shield.net
