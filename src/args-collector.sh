#!/bin/bash

numflags=6 # Total number of possible flags
count=-1 # Counter for order of flags
ipos=$numflags ppos=$numflags npos=$numflags vpos=$numflags rpos=$numflags xpos=$numflags # Initiate positional value of each flag
#              0:1:2:34:5  Position of flags in the `flagpos` vector below
while getopts :i:p:n:vr:x option; do # Colon ':' tells the script that an argument will be passed alongside the flag 
	count=$((count+1)) # Add one to the counter
	case $option in
		i) 
			if [[ $@ != *"-p"* ]]; then
				echo "the flag -p is required with the name of the package"
				exit
			fi
			incmd=$OPTARG
			ipos=$count # Note the position of the flag (i.e, the order in which the flags have been inputted
			;;
		p) 
			if [[ $@ != *"-n"* ]] && [[ $@ == *"-i"* ]]; then
				echo "the flag -n is required with a note of the package"
				exit
			fi
			package=$OPTARG
			ppos=$count
			;;
		n) 
			note=$OPTARG
			npos=$count
			;;
		v) 
			vpos=$count
			;;
		r) 
			if [[ $@ != *"-p"* ]]; then
				echo "the flag -p is required with the name of the package"
				exit
			fi
			rmcmd=$OPTARG
			rpos=$count
			;;
		x) 
			if [[ $@ != *"-p"* ]]; then
				echo "the flag -p is required with the name of the package"
				exit
			fi
			xpos=$count
			;;
	esac
done


if [[ -n $incmd ]]; then # Check if install argument was passed
	eval $incmd $package; # Install the package
fi

if [[ -n $rmcmd ]]; then # Check if remove argument was passed
	eval $rmcmd $package; # Uninstall the package
fi

flagpos=($ipos $ppos $npos $vpos $rpos $xpos) # Order in which the flags occur
shiftpos=(1 1 1 0 1 0) # 1 for flag takes an argument, 0 if flag takes no argument
/usr/lib/notepac/note-manager $numflags ${flagpos[*]} ${shiftpos[*]} "$@" # Quote marks to pass arguments with a space in as one argument rather than two in rust
