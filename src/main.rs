use std::{
    fs::{write, OpenOptions, File},
    io::{Write, BufRead, BufReader},
    env::args,
};

fn main() {
    let homedir = home::home_dir().unwrap().display().to_string(); // Home directory of user
    let notetxt = &format!("{}/notepac.txt", homedir); // Path to text file containing notes
    let args: Vec<String> = args().collect(); // Collect command line (cl) arguments as a Vec of Strings.
    let numflags: usize = args[1].parse().unwrap(); // The number of flags which could possibly be passed (convert the string to an integer)
    let numflags_slice: usize = numflags; // The number of flags as usize, a type which can be passed as an index of a Vec
    let mut flagpos: Vec<usize> = args[2.. 2+numflags_slice].to_vec().into_iter().map(|x| x.parse().unwrap()).collect(); // The 'flagpos' vector from bash, the Slice of the args Vec has to be iter(atively) map(ped) to a Vec of usize(s)
    let shiftpos: Vec<usize> = args[2+numflags_slice.. 2+2*numflags_slice].to_vec().into_iter().map(|x| x.parse().unwrap()).collect(); // The 'shift' vector from bash
    let np_flags:Vec<String> = args[2+2*numflags_slice..].to_vec(); // Flags and arguments passed by user to notepac
    let mut flagargs: Vec<String> = vec!["".to_string(); numflags]; // Initialise vector of arguments which the user passed (without the flags)
    let mut shifty: usize = 0; // Initialise integer which tells the script the index of the next flag argument contained in in np_flags 
    for _i in 0..flagpos.len() {
        let index = flagpos.iter().position(|x| x == flagpos.iter().min().unwrap()).unwrap(); // Iterate over the flagpos vector from min to max value (first to last flag passed by the user, then all the flags that weren't passed have value `numflags`)
        if flagpos[index] == numflags {break} // The indexed flag was not passed
        else { // The indexed flag was passed by the user
            shifty += shiftpos[index]; // Shift the np_flags indexer to the argument of the flag (rather than the flag itself, if the flag should be passed alongside an argument)
            flagargs[index] = np_flags[shifty].to_string(); // Assign the argument of the flag (or flag itself if no arguments are required) to its position in the flagargs vector 
            flagpos[index] = numflags; // The flagpos element associated with this flag now has the value `numflags`, meaning on the next iteration it will be 'skipped' (it will no longer be the smallest value, but instead tied largest possible value)
            shifty += 1; // Shift the np_flags indexer by 1 (to the next user passed flag)
        };
    };

    // Describe each flag argument possibly passed by the user
    let package = &flagargs[1]; 
    let note = &flagargs[2]; let v_flag = &flagargs[3];
    let rmcmd = &flagargs[4]; let x_flag = &flagargs[5];

    if note != "" { // A note was passed
        wa_file(notetxt, package, note) }; // Write the note to the file
    if v_flag == "-v" { // User wants to view a note made about the package 
        vw_note(notetxt, package) }; // Output the note to the user, if it exists
    if x_flag == "-x" || rmcmd != "" { // User wants to delete the note made about the package
        rm_note(notetxt, package) }; // Remove the note
}


fn vw_note(notetxt: &String, package : &String) {
    let file = File::open(notetxt) // Open file
        .expect("Error opening file, please check if the note file does exists at ~/notepac.txt");
    let reader = BufReader::new(file); // Read file to buffer
    let lines = reader.lines(); // Extract lines from the buffer
    let pacstring = format!("{} =", package); // The string which a pre-existing package in the file should match
    // Suggestion: perhaps faster to read from bottom to top.
    // Find the line which corresponds to the passed package
    let mut linefound = 0;
    for line in lines { // Loop over the lines in the file
        let line = line.unwrap(); // Line as String rather than String literal (e.g 'firefox = web browser' rather than 'OK("firefox = "web browser")'
        if line.contains(&pacstring) { // Package is found in the line
            let note_index_start = line.find("=").unwrap(); // The equals sign '=' separates the package and the note, so the note will certainly come after this sign  
            let written_note = &line[note_index_start + 2..]; // The note associated with the package
            println!("{}", written_note);
            linefound = 1;
            break;
        }
    }
    if linefound == 0 {
            println!("no note found for {}", package);
    }
}

// Append data to a file or write data to a new file.
fn wa_file(notetxt: &String, package: &String, note: &String) {
    let data = format!("\n{} = {}", package, note); // Data to be written to the file
    let mut file = OpenOptions::new() // Create the mut(able) variable 'file', as a file to be operated on
        .append(true) // Append data to the file if it does exist
        .create(true) // Create the file if it doesn't exist
        .open(notetxt) // (attempt to) open the file at such a path
        .expect("Unable to open file"); // Error message to display if error occurs
    file.write_all(data.as_bytes()) // Write data to the file
        .expect("Unable to write to file");
    println!("Entry created");
}


// Check if the package is in the file, and if it is, return the index of the package (the line the
// package is on)
fn check_pac(notetxt: &String, package: &String) -> usize {
    let file = File::open(notetxt) // Open file
        .expect("file error");
    let reader = BufReader::new(file); // Read file to buffer
    let lines = reader.lines(); // Extract lines from the buffer
    let pacstring = format!("{} =", package); // The string which a pre-existing package in the file should match
    // Suggestion: perhaps faster to read from bottom to top.
    // Find the line which corresponds to the passed package
    for (index, line) in lines.enumerate() { // Loop over the lines in the file
        let line = line.unwrap(); // Line as String rather than String literal (e.g 'firefox = web browser' rather than 'OK("firefox = "web browser")'  
        if line.contains(&pacstring) { // Package is found in the line
        // Show the line and its number.
            let pacline_i = index + 1; // The line the package is found on
            println!("Entry exists");
            return pacline_i; // Pass the index of the line the package is on
        }
    }
    return 0; // If no index is found, pass the value 0 (which is impossible for 'pacline_i to have
}

// Remove the note associated with the package
fn rm_note(notetxt: &String, package: &String) {
    let pacline_i = check_pac(notetxt, package); // Index of the line the package is on
    if !(pacline_i == 0) { // The package was found in the file
        let samefile = OpenOptions::new()
            .read(true)
            .open(notetxt)
            .expect("doesn't exist");
         let mut lines: Vec<String> = BufReader::new(samefile).lines() // Read the lines of the file to the buffer
             .map(|x| x.unwrap())
             .collect(); // Get the lines as a vector of strings
         lines.remove(pacline_i - 1); // Remove the line which contained the package
         let less_lines = lines.join("\n"); // Concatenate each element of the vector as a string seperated by carriage returns ("\n")
         write(notetxt, less_lines).expect("can't write lines");
         println!("Entry removed");
    } // Write the lines of the file (without the line containing the package) to the file.
}   
