## Terminology

The terms *package*, *application*, and *software* are used interchangeably.

# Description

`notepac` allows users to make notes about applications on their system, especially useful for those rarely used, or obscurely named applications.

# Background

Built as a tool to help users keep track of the purpose of their applications, using `notepac` can aid in preventing the accidental uninstallation of necessary software, and the identification of potentially unnecessary or even problematic software.

Thus `notepac` has the following applications:
- De-bloating
- Personalising the purpose of packages to the system
- Troubleshooting

# Installation

## Arch Linux

Using an AUR helper (such as `yay` or `paru`):

```
paru notepac
```

## From scratch (For Linux, Mac and BSD)

### Pre-requisites

Please install `bash`, `git`, `cargo` and `pandoc` to successfully install `notepac`. You only need to keep `bash` after you have installed `notepac`.

### Step by step installation

Enter the following commands to the terminal:

1. Change directory to your home directory

```
cd
```

2. Clone this repository
```
git clone https://gitlab.com/vaba/notepac.git
```
3.  Change directory to the *notepac* directory (where the repository has been cloned to)
```
cd notepac
```
4. Convert the rust script to an executable
```
cargo build --release
```
5. Make the directory to host the manual page in
```
mkdir -p target/release/man		
```
6. Convert the manual page from markdown format to normal manpage format
```
pandoc --standalone -f markdown -t man man/notepac.1.md > \
"target/release/man/notepac.1"
```
7. Copy the bash script to a binary directory, and convert it to an executable so the command `notepac` can be used system wide (also setting the appropriate read, write and execute permissions
```
install -Dm755 --no-target-directory \
"src/args-collector.sh" "/usr/bin/notepac" 
```
8. Copy the rust executable to a valid directory for an EFL (executable) file, and set appropriate read, write and execute permissions
```
install -Dm755 --target-directory="/usr/lib/notepac" \
"target/release/note-manager" 
```
9. Copy the manpage to the appropriate directory where it may be accessed by the command `man notepac`, and set appropriate read, write and execute permissions
```
install -Dm644 --target-directory="/usr/share/man/man1" \
"target/release/man/notepac.1" 
```
You're done!

### All-in-one install

This installation is the same as the step-by-step installation, but you can just copy and paste the whole block in one go. You may be asked for elevated privileges, and this may cause other commands not yet parsed by the terminal to not work. If this is the case, find the last command that worked successfully and start from that point in the step-by-step guide instead.

```
cd
git clone https://gitlab.com/vaba/notepac.git
cd notepac
cargo build --release
mkdir -p target/release/man		
pandoc --standalone -f markdown -t man man/notepac.1.md > \
"target/release/man/notepac.1"
install -Dm755 --no-target-directory \
"src/args-collector.sh" "/usr/bin/notepac" 
install -Dm755 --target-directory="/usr/lib/notepac" \
"target/release/note-manager" 
install -Dm644 --target-directory="/usr/share/man/man1" \
"target/release/man/notepac.1" 
```

### Uninstallation

```
cd
sudo rm -rf pacman /usr/lib/notepac
sudo rm /usr/bin/notepac /usr/share/man/man1/notepac.1
```

# How to use

See the [manual](man/notepac.1.md).
